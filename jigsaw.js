var svg = d3.select('svg');
var rects = svg.selectAll('rect');

var units = svg.selectAll('g'); // container for each jigsaw unit

var colours = d3.scale.category10();

var colourIndex = 0;
//////////
// init //
//////////

var defA = setJigsawPiece("A's title",'');
var defB = setJigsawPiece("B's title",'');
var defC = setJigsawPiece("C's title",'');

var testObj = setJigsawData("大造じいさんとがん",defA,defB,defC);

var jigsawData = new Array(); // contains jigsaw units

////////////////
// parameters //
////////////////
var param = {
    rects: {
	width: 400,
	height: 80,
	padding:   {
	    x: 0, y: 25
	},
	colour: 'SkyBlue'
    },
    texts: {
	x: 50,
	y: 45,
	colour: 'black'
    },
    pieces: {
	width: 400/3, // 1/3 of 'rects.width'
	height: 80, // same as 'rects.height'
	getColour: function(){return colours(colourIndex++);},
	textColour: 'white',
	textPaddings: {
	    x: 10, y: 15
	}
    }
}

//////////////
// end init //
//////////////

function setJigsawData (theme,a,b,c){
    return {theme: theme, pieces: [a, b, c]};
}

function setJigsawPiece(title,content){
    return {title: title, content: content};
}

function appendJigsaw(jigsaw){
    units = svg.selectAll('.unit');
    jigsawData.push(jigsaw);
    var jigsawUnit = units.data(jigsawData).enter().append('svg:g');

    var piecelist = jigsaw.pieces;
    
    // mouse events for unit
    // jigsawUnit

    // draw rects
    jigsawUnit
	.classed('unit',true)
	.attr('transform',function(d,i) { return 'translate(0,' + (param.rects.padding.y + param.rects.height)*i+')'; } )
    
    jigsawUnit
	.append('rect')
	.attr('width',param.rects.width)
	.attr('height',param.rects.height)
	.attr('x', 0)
	.attr('y', 0)
	.style('fill',param.rects.colour)
    
    jigsawUnit
	.append('svg:text')
	.attr('x', param.texts.x)
	.attr('y', param.texts.y)
	.text(function(d) { return d.theme; });

    jigsawUnit
	.classed('visible',true)
        .on('mouseover', toggle_visible)
	.on('mouseout', toggle_visible);
    
    var pieces = jigsawUnit.selectAll('.piece').data(piecelist).enter().append('svg:g')
	.classed('piece',true)
	.classed('visible',false) // default visibility
	.style('opacity',0) // needed because 'toggle_visible' is not executed for loading
    	.attr('transform', function (d,i){return 'translate('+i*param.rects.width/3+',0)'});//+param.rects.padding.y+')'});

    pieces.append('rect')
	.attr('width',param.pieces.width)
	.attr('height',param.pieces.height)
//	.style('fill',param.pieces.getColour()); // 'getColour' is only called once!
	.style('fill',function(d,i) {return param.pieces.getColour();});

    pieces.append('svg:text')
	.attr('x',param.pieces.textPaddings.x)
    	.attr('y',param.pieces.textPaddings.y)
	.style('fill',param.pieces.textColour)
	.text(function(d){return d.title;});
}

function mousedown(d){
    var selected = d3.select(this);
    selected.transition()
    .style('opacity',0.1);
    
    var pieces = selected.selectAll('.piece').data(d.piece).enter();
    pieces.append('g')
    .style('fill','green')
    .classed('piece',true);

    
}

function toggle_visible(){
    // select pieces
    var pieces = d3.select(this).selectAll('g.piece');
    // change cursor style
    pieces.style('cursor','pointer');

    // get current visibility
    var visible = pieces.classed('visible');

    if(visible){
	// make it invisible
    	pieces.transition().style('opacity',0.1);
    }else{
	// make it visible
    	pieces.transition().style('opacity',1);
    }

    // if visible, make it invisible
    pieces.classed('visible', !visible); 

}
// function mouseover(d){
    
// }


appendJigsaw(testObj);
